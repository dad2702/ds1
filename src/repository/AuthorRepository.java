package repository;

import java.util.List;

import model.Author;

public interface AuthorRepository {
	
	void addAuthor(String title, String description, String price, String pubDate);
	List<Author> listAuthor();
}
