package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Book;
import sun.rmi.server.Dispatcher;

/**
 * Servlet implementation class BookServlet
 */
@WebServlet("/book/*")
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	ArrayList<Book> miArray = new ArrayList();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getRequestURI().equals("/ds1/book")){
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/Book/index.jsp");
			dispatcher.forward(request, response);
		} else if (request.getRequestURI().equals("/ds1/book/new")){
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/Book/create.html");
			dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		processRequest(request, response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre = request.getParameter("title");
		String apellidos = request.getParameter("author");
		String pages = request.getParameter("pages");
		Book miLibro = new Book(nombre, apellidos, pages);
		miArray.add(miLibro);
		HttpSession session = request.getSession();
		session.setAttribute("books", miArray);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/Book/index.jsp");
		dispatcher.forward(request, response);
	}
}


