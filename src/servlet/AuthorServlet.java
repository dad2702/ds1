package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Author;

/**
 * Servlet implementation class AuthorServlet
 */
@WebServlet("/author/*")
public class AuthorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private List <Author> autores;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AuthorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());
		if("/ds1/author/new".equals(request.getRequestURI())){
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/Author/create.html");
			rd.forward(request, response);
		}else{	        
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/Author/index.jsp");
			rd.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		  
    	String id = request.getParameter("id");
        String nombre = request.getParameter("name");
        String apellidos = request.getParameter("surname");
        String nacimiento = request.getParameter("birthdate");
    	Author author = new Author(id, nombre, apellidos, nacimiento);
        HttpSession sesion = request.getSession();

        if(sesion.getAttribute("autores") == null){
            autores = new ArrayList();
            autores.add(author);
            sesion.setAttribute("autores", autores);      
        }else{
            autores = (ArrayList) sesion.getAttribute("autores");
            autores.add(author);
        }       
		response.sendRedirect("/ds1/author");
	}

}
